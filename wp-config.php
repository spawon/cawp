<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '8|72C1-mMlO%{!$~cf~=!i-Qn$!YYW>5|GH&r-2$4,cX2`dn<i8Oye)*=j^{oz]v' );
define( 'SECURE_AUTH_KEY',  '&H4>.1AJIe?zyh*G[zGPy}#Ox6b}/o_Y~7m1_e;o}XzgIce%Gmo[.|5H{~hIBLXQ' );
define( 'LOGGED_IN_KEY',    'L7w;WHB}#sw%!Si)^_S)/~OArc1f>oq@GZ;z-1 RVyyE)>p>#js5{YKq&K(X=>&W' );
define( 'NONCE_KEY',        '2dB8o9_&0py:Sx!;W4(Ga!&LAmeBIXi~|yK@*90?g`=AmN.U6,a^1$3T?g3-QsI<' );
define( 'AUTH_SALT',        '-|Gg<Rw6uAF]j;zWCC@iY]9qi8czq XHMVTkN^/rT6GFWvkHJY(s1[XeY8`1fv_z' );
define( 'SECURE_AUTH_SALT', 'x:4&Q^MM;TUtG/j>,M].txKt=}/+PM{@}6;v%G}0O,z5_x[?mtVaVXfT2tkq]osq' );
define( 'LOGGED_IN_SALT',   'V.L/OqV9XWI}?F#}b}WxDA`zK@DzTDyd14S}V<%?W?r&zg9mg|L 7UL=nT02$nvQ' );
define( 'NONCE_SALT',       'd1yG<l&D*jv,6q_^@D6USVW1hQWN*N NoseKIS9L#_]+[jw:m^ypu7|W,9hB:8N@' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
