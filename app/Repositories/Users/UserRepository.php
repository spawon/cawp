<?php


namespace App\Repositories\Users;


/**
 * User repository
 * class for job with database at user
 *
 * Class UserRepository
 * @package App\Repositories\Users
 */
class UserRepository
{
    private $connection;

    /**initial connect with mysql
     * UserRepository constructor.
     * @param $connection
     */
    public function __construct($connection)
    {
        $this->connection = $connection;
    }

    /**
     * select user by id
     * @param $id
     */
    public function getUser($id){

    }

    /**
     *select user list
     */
    public function getUserList(){

    }

    /**
     * creating user
     *
     * @param array $info
     * @return int
     */
    public function createUser(array $info){
        $result = $this->connection->query("INSERT INTO `users` (`name`, `password`) VALUES ('".$info['name']."', '".$info['password']."')");
        if (!$result) {
            printf("Insert error: %s\n", $this->connection->error);
            die();
        }
        return 1;

    }

    /**
     *deleting user
     */
    public function deleteUser(){

    }
}