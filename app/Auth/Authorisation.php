<?php

namespace App\Auth;
use App\Repositories\Users\UserRepository;

// connecting config wordpress database
require($_SERVER['DOCUMENT_ROOT'] . '/wp-config.php');

/**
 * abstract class for job with authorisation and registration
 *
 * Class Authorisation
 * @package App\Auth
 */
abstract class Authorisation
{
    /**
     * @var string
     */
    protected $dbHost;
    /**
     * @var string
     */
    protected $dbLogin;
    /**
     * @var string
     */
    protected $dbPassword;
    /**
     * @var string
     */
    protected $dbName;

    /**
     * @var UserRepository
     */
    public static $userRepository;

    /**
     * import and save properties for database mysql
     * Authorisation constructor.
     */
    public function __construct()
    {
        $this->dbHost = DB_HOST;
        $this->dbLogin = DB_USER;
        $this->dbPassword = DB_PASSWORD;
        $this->dbName = DB_NAME;
        self::$userRepository = new UserRepository($this->connect());

    }

    /**
     * initial connect with mysql
     * @return \mysqli
     */
    private function connect()
    {
        $mysqli = new \mysqli($this->dbHost, $this->dbLogin, $this->dbPassword, $this->dbName);
        if (mysqli_connect_errno()) {
            printf("Connect to MySQL Error. Error code: %s\n", mysqli_connect_error());
            exit;
        }
        return $mysqli;
    }


}