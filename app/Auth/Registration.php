<?php

namespace App\Auth;


/**
 * Main registration class
 * validate and register user
 *
 * Class Registration
 * @package App\Auth
 */
class Registration extends Authorisation
{

    /**
     * add user function
     * validate append new user
     * @param array $info
     */
    public function addUser(array $info)
    {
        $info['password'] = md5($info['password']);
        self::$userRepository->createUser($info);
    }

}